using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;



public class gallery : MonoBehaviour {
    // Start is called before the first frame update

    public GameObject Waiting_Panel;

    bool loading_of_images_done;

    List<GameObject> SlotItems = new List<GameObject> ();

    public List<GameObject> Decks = new List<GameObject> ();

    List<card> GalleryCards = new List<card> ();

    int k;

    bool SaveFile_loaded;

    public TextAsset cards_json;

    int TotalNumber_of_Cards = 50;

    string OS_Path;

    void OnGUI () {
        MouseUI.CallOnGUI ();
    }

    void Start () {

        if (Specs.Android)
            OS_Path = Application.persistentDataPath;
        else
            OS_Path = Application.dataPath;

        SaveFile_loaded = false;

        k = 0;

        for (int i = 0; i < 10; i++)
            SlotItems.Add (transform.GetChild (0).GetChild (i).GetChild (0).gameObject);

        loading_of_images_done = false;

        StartCoroutine (load_setting ());

    }


    // Update is called once per frame
    void Update () {

        if (Input.GetKeyUp (KeyCode.Escape)) {
            StartCoroutine (save_game_status_and_return_to_home ());
            
        }

    }




    IEnumerator load_images () {

        string jsonString = cards_json.text;

        card_class cardsInJson = JsonConvert.DeserializeObject<card_class> (jsonString);

        Texture2D texture_2d;

        string path = OS_Path + "/Cards/";

        if (!Directory.Exists (path)) {

            Directory.CreateDirectory (path);

            for (int i = 0; i < TotalNumber_of_Cards; i++) {

                UnityWebRequest www = UnityWebRequestTexture.GetTexture (
                    cardsInJson.data[i].images.small);

                yield return www.SendWebRequest ();

                if (www.result != UnityWebRequest.Result.ConnectionError &&
                    www.result != UnityWebRequest.Result.ProtocolError) {

                    texture_2d = (Texture2D)((DownloadHandlerTexture)www.downloadHandler).texture;

                    byte[] bytes = texture_2d.EncodeToPNG ();
                    File.WriteAllBytes (path + "Image" + i + ".png", bytes);

                    Sprite sprite_ = Sprite.Create (texture_2d,
                        new Rect (0, 0, texture_2d.width, texture_2d.height),
                        new Vector2 (0.5f, 0.5f));

                    GalleryCards.Add (new card (i, sprite_, texture_2d, cardsInJson.data[i],
                        _already_drawn: false));

                }
            
            }

        } else {

            for (int i = 0; i < TotalNumber_of_Cards; i++) {

                var rawData = System.IO.File.ReadAllBytes (Path.Combine (path, "Image" + i + ".png"));
                texture_2d = new Texture2D (2, 2);
                texture_2d.LoadImage (rawData);

                Sprite sprite_ = Sprite.Create (texture_2d,
                    new Rect (0, 0, texture_2d.width, texture_2d.height),
                    new Vector2 (0.5f, 0.5f));

                GalleryCards.Add (new card (i, sprite_, texture_2d, cardsInJson.data[i],
                    _already_drawn: false));

            }




          
        }

        loading_of_images_done = true;

    }



    IEnumerator load_setting () {

        Waiting_Panel.SetActive (true);
     
        StartCoroutine (load_images ());

        yield return new WaitUntil (() => loading_of_images_done);

        if (File.Exists (Path.Combine(OS_Path + "/Data/","Save.txt"))) {
            StartCoroutine (read_data_from_save_file ());
            yield return new WaitUntil (() => SaveFile_loaded);
        }

        Waiting_Panel.SetActive (false);
           
        load_gallery ();

    }


    IEnumerator save_game_status_and_return_to_home () {

        string path = OS_Path + "/Data/Save.txt";

        if (!Directory.Exists (OS_Path + "/Data"))
            Directory.CreateDirectory (OS_Path + "/Data");

        StreamWriter writer = new StreamWriter (path, append: false);

        writer.WriteLine ("k");
        writer.WriteLine (k);
        
        writer.WriteLine ("Undrawable Cards");

        bool found_drawn_card = false;

        for (int i = 0; i < GalleryCards.Count; i++)
            if (GalleryCards[i].already_drawn) {
                found_drawn_card = true;
                writer.WriteLine (i);
            }

        if (!found_drawn_card)
            writer.WriteLine ("none");

        for (int i = 0; i < Decks.Count; i++) {

            writer.WriteLine ("Deck " + i);

            if (Decks[i].GetComponent<Deck> ().CardsOn.Count > 0)
                for (int j = 0; j < Decks[i].GetComponent<Deck> ().CardsOn.Count; j++)
                    writer.WriteLine (Decks[i].GetComponent<Deck> ().CardsOn[j].SN);
            else
                writer.WriteLine ("empty");
        }

        writer.Close ();

        writer = null;

        yield return new WaitUntil (() => writer == null);

        SceneManager.LoadScene ("Home", LoadSceneMode.Single);

    }


    IEnumerator read_data_from_save_file () {

        string[] transcript_line = File.ReadAllLines (OS_Path + "/Data/Save.txt");
        List<string> lines = new List<string> (transcript_line);

        int.TryParse (lines[1], out int k_value);
        k = k_value;

        int i = 4;

        if (!lines[3].Contains ("none")) {

            while (!lines[i].Contains ("Deck 0")) {
                
                int.TryParse (lines[i], out int index_of_undrawableCard);

                GalleryCards[index_of_undrawableCard].make_undrawable ();
                
                i++;

            }

            int Deck_Number = -1;

            while (i < lines.Count) {

                if (lines[i].Contains ("Deck "))
                    Deck_Number++;

                if (int.TryParse (lines[i], out int index_of_deckCard))
                    Decks[Deck_Number].GetComponent<Deck> ().AddCardOn (GalleryCards[index_of_deckCard]);                

                i++;

            }

        }

        yield return new WaitUntil (() => i == lines.Count);

        SaveFile_loaded = true;

    }


    void load_gallery () {

        for (int i = 0; i < SlotItems.Count; i++)
            SlotItems[i].GetComponent<galleryItem> ().assign (GalleryCards[10 * k + i]);

    }


    public void show_next_bundle_of_cards () {
        k++;
        if (k == GalleryCards.Count / 10)
            k = 0;
        load_gallery ();
    }


    public void show_previous_bundle_of_cards () {
        k--;
        if (k == -1)
            k = GalleryCards.Count / 10 - 1;
        load_gallery ();
    }


}




