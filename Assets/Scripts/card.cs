using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class card {

    public int SN;
    public Sprite sprite;
    public Texture2D texture2d;
    public data_class data;
    public bool already_drawn;

    public card (int _SN, Sprite _sprite, Texture2D _texture2d, data_class _data,
        bool _already_drawn) {

        SN = _SN;
        sprite = _sprite;
        texture2d = _texture2d;
        data = _data;
        already_drawn = _already_drawn;

    }

    public void make_undrawable () {

        already_drawn = true;

    }

}
