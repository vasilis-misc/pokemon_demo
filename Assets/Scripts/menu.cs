﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.SceneManagement;


public class menu : MonoBehaviour {
    
    // Start is called before the first frame update
    void Start () {
        
    }

    public string next_scene (int _choice) {

        for (int i = 0; i < Scenes.Scene_Transitions.Count; i++)
            if (_choice == Scenes.Scene_Transitions[i].Choice &&
                SceneManager.GetActiveScene ().name == Scenes.Scene_Transitions[i].FirstScene)
                return Scenes.Scene_Transitions[i].SecondScene;

        return "void";

    }


    public string previous_scene () {

        for (int i = 0; i < Scenes.Scene_Transitions.Count; i++)
            if (SceneManager.GetActiveScene ().name == Scenes.Scene_Transitions[i].SecondScene)
                return Scenes.Scene_Transitions[i].FirstScene;

        return "void";

    }


    public void Update () {
        if (Input.GetKeyUp (KeyCode.Escape))
            go_back ();
    }


    public void go_forward (int _choice) {

        SceneManager.LoadScene (next_scene (_choice), LoadSceneMode.Single);

    }


    public void go_back () {
        
        string Current_Scene = SceneManager.GetActiveScene ().name;

        if (Current_Scene == "Home")
            Application.Quit ();
        else
            SceneManager.LoadScene(previous_scene (), LoadSceneMode.Single);

    }


}
