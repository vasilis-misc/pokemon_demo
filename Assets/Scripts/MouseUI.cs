using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseUI : MonoBehaviour {

    public static GameObject OnFocusCard;

    public static GameObject CardBeingDragged;
    public static GameObject Deck_that_CardIsBeingDraggedOver;

    private static Event event_;

    private static Texture2D DragIcon;


    // Start is called before the first frame update
    void Start () {

        OnFocusCard = GameObject.Find ("CardOnFocusPanel").transform.
            GetChild (0).GetChild (0).GetChild (0).gameObject;

        CardBeingDragged = null;
        Deck_that_CardIsBeingDraggedOver = null;

    }

    // Update is called once per frame
    void Update () {

    }

    public void OnMouseEnter () {
        if (CardBeingDragged == null) {
            if ((GetComponent<galleryItem> () && !GetComponent<galleryItem> ().GalleryCard.already_drawn)
                || (GetComponent<Deck> () && GetComponent<Deck> ().CardsOn.Count > 0)) {
                OnFocusCard.GetComponent<Image> ().color = Color.white;
                OnFocusCard.GetComponent<Image> ().sprite = GetComponent<Image> ().sprite;
            }
        } else if (CardBeingDragged != null && GetComponent<Deck> ())
            Deck_that_CardIsBeingDraggedOver = gameObject;
    }

    public void OnMouseExit () {
        OnFocusCard.GetComponent<Image> ().color = new Color (1F, 1F, 1F, 0F);
        OnFocusCard.GetComponent<Image> ().sprite = null;
        if (Deck_that_CardIsBeingDraggedOver != null)
            Deck_that_CardIsBeingDraggedOver = null;
    }


    public void OnMouseDrag () {
        if ((Specs.Android && Input.GetTouch (0).phase == TouchPhase.Moved) ||
            (!Specs.Android && event_.isMouse)) {
            if ((GetComponent<galleryItem> () && !GetComponent<galleryItem> ().GalleryCard.already_drawn)
            || (GetComponent<Deck> () && GetComponent<Deck> ().CardsOn.Count > 0)) {
                CardBeingDragged = gameObject;
                if (GetComponent<galleryItem> ())
                    DragIcon = GetComponent<galleryItem> ().GalleryCard.texture2d;
                else //Deck card
                    DragIcon = GetComponent<Deck> ().TopCard ().texture2d;
            }
        }

    }


    public void OnMouseUp () {

        if (CardBeingDragged != null && Deck_that_CardIsBeingDraggedOver != null
            && CardBeingDragged != Deck_that_CardIsBeingDraggedOver) {
            change_place ();
            CardBeingDragged = null;
            Deck_that_CardIsBeingDraggedOver = null;
        }

        DragIcon = null;

    }


    public void OnMouseDown () {

        if (Specs.Android && Input.GetTouch (0).phase == TouchPhase.Stationary) {

            if (OnFocusCard == null)
                OnMouseEnter ();

        }
    
    }


    public void OnMouseUpAsButton () {

        if (Specs.Android && Input.GetTouch (0).phase == TouchPhase.Ended) {

            if (OnFocusCard != null)
                OnMouseExit ();

        }

    }


    public void change_place () {

        if (CardBeingDragged.GetComponent<Deck> ()) {

            Deck_that_CardIsBeingDraggedOver.GetComponent<Deck> ().AddCardOn (CardBeingDragged.
                GetComponent<Deck> ().TopCard ());
            CardBeingDragged.GetComponent<Deck> ().RemoveTopCard ();

        } else {//from gallery

            Deck_that_CardIsBeingDraggedOver.GetComponent<Deck> ().AddCardOn (CardBeingDragged.
                GetComponent<galleryItem> ().GalleryCard);
            CardBeingDragged.GetComponent<galleryItem> ().make_undrawable ();
            
        }

    }


    public static void CallOnGUI () {

        event_ = Event.current;

        if (CardBeingDragged != null && DragIcon != null)
            DisplayDragIcon ();

    }


    static void DisplayDragIcon () {

        if (Specs.Android)
            GUI.DrawTexture (new Rect (Input.GetTouch (0).position.x,
                Screen.height - Input.GetTouch (0).position.y, 50, 80), DragIcon);
        else
            GUI.DrawTexture (new Rect (event_.mousePosition.x - 50,
                event_.mousePosition.y - 10, 50, 80), DragIcon);

    }

}
