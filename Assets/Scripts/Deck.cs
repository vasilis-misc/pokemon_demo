using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Deck : MonoBehaviour {

    public static Sprite blank_card;

    public List<card> CardsOn;

    // Start is called before the first frame update
    void Start () {

        blank_card = GetComponent<Image> ().sprite;

        CardsOn = new List<card> ();

    }


    public void AddCardOn (card _card) {
        CardsOn.Add (_card);
        update_top_sprite ();
    }


    public void RemoveTopCard () {
        CardsOn.RemoveAt (CardsOn.Count - 1);
        update_top_sprite ();
    }

    public void update_top_sprite () {

        if (CardsOn.Count == 0)
            GetComponent<Image> ().sprite = blank_card;
        else
            GetComponent<Image> ().sprite = CardsOn[CardsOn.Count - 1].sprite;

    }


    public card TopCard () {

        return CardsOn[CardsOn.Count - 1];

    }

}
