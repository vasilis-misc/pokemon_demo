﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;
using System.Linq;

public class loading_screen : MonoBehaviour {

	public Image splashImage;
	public Image BGImage;

	IEnumerator Start () {

		splashImage.canvasRenderer.SetAlpha (0.0f);
		BGImage.canvasRenderer.SetAlpha (0.0f);

		BGFadeIn ();
		yield return new WaitForSeconds (1.0f);
		SplashFadeIn ();
		yield return new WaitForSeconds (2.0f);
		SplashFadeOut ();
		yield return new WaitForSeconds (1.0f);
		BGFadeOut ();
		yield return new WaitForSeconds (1.5f);

		SceneManager.LoadScene ("Home", LoadSceneMode.Single);

	}

	void BGFadeIn () {
		BGImage.CrossFadeAlpha (1.0f, 1.0f, false);
	}

	void SplashFadeIn () {
		splashImage.CrossFadeAlpha (1.0f, 1.0f, false);
	}

	void SplashFadeOut () {
		splashImage.CrossFadeAlpha (0.0f, 1.0f, false);
	}

	void BGFadeOut () {
		BGImage.CrossFadeAlpha (0.0f, 1.5f, false);
	}





}



