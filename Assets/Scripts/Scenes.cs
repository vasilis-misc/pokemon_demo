﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenes : MonoBehaviour {

    public class SceneTransition {

        public int Edition;
        public string FirstScene;
        public int Choice;
        public string SecondScene;

        public SceneTransition (string _FirstScene, int _Choice, string _SecondScene) {

            FirstScene = _FirstScene;
            Choice = _Choice;
            SecondScene = _SecondScene;

        }

    }


    public static List<SceneTransition> Scene_Transitions = new List<SceneTransition> {

        new SceneTransition ("Home", 0, "DeckBuilder"),
        new SceneTransition ("Home", 1, "About"),
 
    };



}
