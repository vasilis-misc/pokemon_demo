using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class galleryItem : MonoBehaviour {

    public card GalleryCard;

    void Start () {

    }

    public void assign (card _card) {

        GalleryCard = _card;

        GetComponent<Image> ().sprite = _card.sprite;

        if (!_card.already_drawn)
            GetComponent<Image> ().color = Color.white;
        else
            GetComponent<Image> ().color = new Color (1F, 1F, 1F, 0.2F);        

    }

    public void make_undrawable () {
        GalleryCard.make_undrawable ();
        GetComponent<Image> ().color = new Color (1F, 1F, 1F, 0.2F);
    }

}


