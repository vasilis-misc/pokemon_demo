using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ability_class {

    public string name;
    public string text;
    public string type;

}

public class attack_class {

    public List<string> cost;
    public string name;
    public string text;
    public string damage;
    public int convertedEnergyCost;

}

public class weakness_class {

    public string type;
    public string value;

}

public class resistance_class {

    public string type;
    public string value;

}

public class legality_class {

    public string standard;
    public string expanded;
    public string unlimited;

}

public class set_image_class {

    public string symbol;
    public string logo;

}

public class image_class {

    public string small;
    public string large;

}

public class set_class {

    public string id;
    public string name;
    public string series;
    public int printedTotal;
    public int total;
    public legality_class legalities;
    public string ptcgoCode;
    public string releaseDate;
    public string updateAt;
    public set_image_class images;

}


public class prices_euro_class {

    public decimal averageSellPrice;
    public decimal lowPrice;
    public decimal trendPrice;
    public decimal germanProLow;
    public decimal suggestedPrice;
    public decimal reverseHoloSell;
    public decimal reverseHoloLow;
    public decimal reverseHoloTrend;
    public decimal lowPriceExPlus;
    public decimal avg1;
    public decimal avg7;
    public decimal avg30;
    public decimal reverseHoloAvg1;
    public decimal reverseHoloAvg7;
    public decimal reverseHoloAvg30;

}

public class prices_dollar_class {

    public decimal low;
    public decimal mid;
    public decimal high;
    public decimal market;
    public decimal directLow;

}



public class cardmarket_class {

    public string url;
    public string updatedAt;
    public prices_euro_class prices;

}


public class tcgplayer_class {

    public string url;
    public string updatedAt;
    public prices_dollar_class prices;

}


public class ancientTrait_class {

    public string name;
    public string text;

}

public class data_class {

    public string id;
    public string name;
    public string supertype;
    public List<string> subtypes;
    public string level;
    public string hp;
    public List<string> types;
    public string evolvesFrom;
    public List<string> evolvesTo;
    public List<string> rules;
    public ancientTrait_class ancientTrait;
    public List<ability_class> abilities;
    public List<attack_class> attacks;
    public List<weakness_class> weaknesses;
    public List<resistance_class> resistances;
    public List<string> retreatCost;
    public int convertedRetreatCost;
    public set_class set;
    public string number;
    public string artist;
    public string rarity;
    public string flavorText;
    public List<int> nationalPokedexNumbers;
    public legality_class legalities;
    public string regulationMark;
    public image_class images;
    public tcgplayer_class tcgplayer;
    public cardmarket_class cardmarket;

}


public class card_class {

    public List<data_class> data;
    public int page;
    public int pageSize;
    public int count;
    public int totalCount;

}
